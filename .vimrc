set nocompatible " must be the first line
set bs=2
"filetype on
"filetype indent on
"filetype plugin on

set grepprg=grep\ -nH\ $*
"let g:tex_flavor='latex'
set laststatus=2
set statusline=%<%f\%h%m%r%=%-20.(line=%l\ \ col=%c%V\ \ totlin=%L%)\ \ \%h%m%r%=%-40(bytval=0x%B,%n%Y%)\%P
syntax on
set nu
colorscheme dante
"set term=linux
set mouse=a
"autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
set nowrap
set smarttab
"git commit messages
au FileType gitcommit set tw=72
filetype indent plugin on

set nocompatible               " be iMproved
 filetype off                   " required!

 set rtp+=~/.vim/bundle/vundle/
 call vundle#rc()

 " let Vundle manage Vundle
 " required! 
 Bundle 'gmarik/vundle'

 Bundle 'L9'
 Bundle 'FuzzyFinder'
" (HT|X)ml tool
 Bundle 'ragtag.vim'
 filetype plugin indent on     " required!
 "
 " Brief help
" U :BundleList          - list configured bundles
 " :BundleInstall(!)    - install(update) bundles
 " :BundleSearch(!) foo - search(or refresh cache first) for foo
 " :BundleClean(!)      - confirm(or auto-approve) removal of unused bundles
 "
 " see :h vundle for more details or wiki for FAQ
 " NOTE: comments after Bundle command are not allowed..

" FuzzyFinder
" Ignore cabal-dev
let g:fuf_file_exclude = '\v\~$|\.(o|exe|dll|bak|orig|swp)$|(^|[/\\])(\.(hg|git|bzr)|cabal-dev|dist)($|[/\\])'
" Most Recently Used files
nnoremap mru :FufMruFile<CR>
let g:fuf_modesDisable = []
"nnoremap <Leader>e :FuFFile <C-r>=fnamemodify(getcwd(), ':p')<CR><CR>
nnoremap <Leader>e :FufFile <C-r>=expand('%:~:.')[:-1-len(expand('%:~:.:t'))]<CR><CR>
nnoremap <Leader>ff :FufFile<CR>
nnoremap <Leader>, :FufBuffer<CR>
nnoremap <Leader>. :FufTag<CR>
nnoremap <C-h> :FufHelp<CR>

let fuf_abbrevMap  = {
      \   '\C^GVR' : [
      \     '$VIMRUNTIME/**/',
      \     '~/.vim/**/',
      \     '$VIM/.vim/**/',
      \     '$VIM/vimfiles/**/',
      \   ],
      \   '\C^VR' : [
      \     '~/.vim/**/',
      \   ],
      \   '\C^ZZ' : [
      \     '~/.vim/**/zz_',
      \   ],
      \ }


